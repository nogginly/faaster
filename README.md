# Faaster - Quick and easy FaaS micro-services

- [Faaster - Quick and easy FaaS micro-services](#faaster---quick-and-easy-faas-micro-services)
  - [Compatibility](#compatibility)
  - [About](#about)
  - [Example](#example)
  - [Getting Started](#getting-started)
    - [Install via `git` sub-modules](#install-via-git-sub-modules)
    - [Protecting Credentials](#protecting-credentials)
  - [Wrapper Scripts](#wrapper-scripts)
    - [Script `faasteraws.sh`](#script-faasterawssh)
    - [Script `faasterraform.sh`](#script-faasterraformsh)
    - [Pre-conditions](#pre-conditions)
      - [Getting Ready](#getting-ready)
    - [Usage](#usage)
  - [Convenience Scripts](#convenience-scripts)
    - [Script `taint_deployment.sh`](#script-taint_deploymentsh)
    - [Script `faaster-app-init.sh`](#script-faaster-app-initsh)
  - [Module Reference](#module-reference)
    - [Module faaster/aws/api](#module-faasterawsapi)
    - [Module faaster/aws/api_deploy](#module-faasterawsapi_deploy)
    - [Module faaster/aws/api_lambda_auth](#module-faasterawsapi_lambda_auth)
    - [Module faaster/aws/api_lambda_auth_fn](#module-faasterawsapi_lambda_auth_fn)
    - [Module faaster/aws/api_lambda_method](#module-faasterawsapi_lambda_method)
    - [Module faaster/aws/api_lambda_method_fn](#module-faasterawsapi_lambda_method_fn)
    - [Module faaster/aws/api_lambda_role](#module-faasterawsapi_lambda_role)
    - [Module faaster/aws/api_resource](#module-faasterawsapi_resource)
    - [Module faaster/aws/api_stages_lambda_method](#module-faasterawsapi_stages_lambda_method)
    - [Module faaster/aws/api_stages_lambda_method_fn](#module-faasterawsapi_stages_lambda_method_fn)
    - [Module faaster/aws/cron_lambda_fn](#module-faasterawscron_lambda_fn)
    - [Module faaster/aws/crud_dynamo_access](#module-faasterawscrud_dynamo_access)
    - [Module faaster/aws/crud_es_access](#module-faasterawscrud_es_access)
    - [Module faaster/aws/crud_s3_access](#module-faasterawscrud_s3_access)
    - [Module faaster/aws/lambda_fn](#module-faasterawslambda_fn)

## Compatibility

**Faaster** as of 0.13.0 is **ONLY** compatible with `terraform` 0.12.x.

## About 

**Faaster** is a set of `terraform` modules that make it faster to deploy "serverless" microservices using Amazon's Lambda, API Gateway, Dynamo DB and S3 services (AWS).

Lambda functions can be implemented using the following language platforms.

* Node.js using `fn_lang="node"` (or not, since this is the default)
  * Specific versions of Node.js can be used by specifying `node10` or `node8`.
* Go using `fn_lang="go"` 
  * Even though Go has one major version, specific version of Go can be used specifying `go1`.

## Example

The example has been moved to it's own repository: [faaster-example](https://gitlab.com/nogginly/faaster-example)

## Getting Started

### Install via `git` sub-modules

Define your application project folder structure as follows.

```
+-- myapp
    +-- deployments/                <-- where your deployment targets go
    |   +-- deploy_target/
    |       +-- credentials         <-- AWS API access credentials file
    |       +-- terraform.tfvars    <-- Deployment-specific properties
    +-- deps/
    |   +-- faaster/                <-- pull here as a submodule
    +-- modules/
    |   +-- faaster/                 <-- symlink to 'myapp/deps/faaster/modules`
    +-- functions/
    |   +-- node/                   <-- your Node.js functions go under here
    |   |   +-- one_GET/
    |   |   +-- one_POST/
    |   +-- go/                     <-- you Go functions go under here
    |       +-- two_GET/
    |       +-- two_DELETE/
    +-- main.tf                     <-- your main `terraform` config
    +-- README.md
    +-- CHANGELOG.md
    +-- LICENSE
```

Using `git submodule` pull in the `faaster` repo under the `deps/` folder and then symlink the `deps/faaster/modules` as `modules/faaster` so that `faaster` modules show up in your projects `modules/` folder where `terraform` will look for modules that you use.

### Protecting Credentials

> When developing "serverless" app using a `git` repo, it's important to make sure you don't check-in credentials and other important stuff into a public repo somewhere. Take a look at the `.gitignore` file in this repo and feel free to use it as a starting point. It will keep the usual places where creds are put as well as runtime data files etc out of your app's repo.

## Wrapper Scripts

The wrapper scripts provide a consisent way to support multiple credentials when dealing with AWS, either via the AWS CLI or `terraform`.

There are two `terraform` command wrappers, each is identical in terms of how they behave with one exception: they use different top-level namespace folders. If you're wondering why I don't use the `terraform workspace` commands, it's because I wanted different deployments/accounts which in turn use completely different AWS accounts, credentials etc. 

### Script `faasteraws.sh`

This script looks for `TARGET` folders within the `deployments/` namespace folder. 

```plain
Usage: ./bin/faasteraws.sh TARGET AWSSVC AWSCMD [AWSOPTIONS...]

    TARGET - name of configuration target in your deployments/ folder
             with the 'credentials' file used to source in env vars. Required.
    AWSSVC - The 'aws' service name per the AWS CLI reference
    AWSCMD - The service-specific sub-command, followed by its options etc.
```

### Script `faasterraform.sh`

This script looks for `TARGET` folders within the `deployments/` namespace folder. 

```plain
Usage: ./bin/faasterraform.sh TARGET TFCMD [TFOPTIONS...]

    TARGET - name of configuration target in your / folder
             with the 'credentials' file used to source in env vars. Required.
    TFCMD - The command for 'terraform' followed by its options. Supported
            commands are: init, graph, output, state, import, plan,
            apply, destroy
```

### Pre-conditions

1. You need to have an AWS account set up.
2. You need an API key set up, either as a credential for your root account or by setting up an IAM user with admin access.
3. You need the most recent version of [`terraform`](https://terraform.io) installed on your system when using `faasterraform.sh`. 
    - I use `brew` (or `linuxbrew`) to install it and keep it up to date.
4. You need the most recent version of [`aws`](https://docs.aws.amazon.com/cli/latest/index.html) CLI installed on your system when using `faasteraws.sh`. 
    - I use `brew` (or `linuxbrew`) to install it and keep it up to date.
5. Clone this repo.

#### Getting Ready

1. In whichever projects where you plan to use the scripts from, create the following folders:
    - `accounts/` for use with `paasterraform.sh`
    - `deployments/` for use with `faasterraform.sh` and `faasteraws.sh`
2. In whichever namespace you plan to use, create a sub-folder for your target: e.g. `mydeployment/`. Within that account ...
3. Create a file called `credentials` and make sure it exports the following env variables:
    - `export AWS_ACCESS_KEY_ID=YOUR_ACCOUNT_ADMIN_ACCESS_KEY`
    - `export AWS_SECRET_ACCESS_KEY=YOUR_ACCOUNT_ADMIN_SECRET_KEY`
4. Create a file called `terraform.tfvars` and make sure it has the following properties:
    - `aws_region = YOUR_AWS_REGION_IF_NOT_US_EAST_1`
    - `deploy_name = "example"`

### Usage

These wrapper scripts are used to execute the `terraform` or `aws` commands within a particular namespace, so unlike calling them directly the first parameter is the target namespace and then the commands and their parameters.

```bash
$ ./bin/faasterraform mydeployment init
```

## Convenience Scripts

### Script `taint_deployment.sh`

This script uses `faasterraform.sh` to provide a convenient way to "taint" all the AWS resources related to an API deployment whenever you add/update/remove API stages and/or methods.

> TODO more details.

### Script `faaster-app-init.sh`

This script can be used to setup a new `faaster`-based app repo by running with a path. The script will create the directory, initialize it as a `git` repo, and setup folder based on the example pattern so you're ready to start.

> TODO more details

## Module Reference

### Module faaster/aws/api
**INPUTS**

| Name                 | Description                                                                                                                           |  Type  | Default  | Required |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------- | :----: | :------: | :------: |
| api\_desc            | The description of the API under which the function is deployed.                                                                      | string |   n/a    |   yes    |
| api\_name            | The name of the API under which the function is deployed.                                                                             | string |   n/a    |   yes    |
| binary\_media\_types | A list of binary content types that the API will return if you want to limit it in some way.                                          |  list  | `<list>` |    no    |
| deploy\_prefix       | A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only. | string |   n/a    |   yes    |

**OUTPUTS**

| Name     | Description |
| -------- | ----------- |
| id       |             |
| root\_id |             |

### Module faaster/aws/api_deploy
**INPUTS**

| Name                          | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |  Type  | Default  | Required |
| ----------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----: | :------: | :------: |
| api\_id                       | The ID of the API Gateway REST API that is deployed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | string |   n/a    |   yes    |
| deploy\_prefix                | A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | string |   n/a    |   yes    |
| domain\_name                  | Set this attribute to the name of domain to map the API deployment to. The domain must already be available as a Route53 hosted zone, and an AWS Certficate **in `us-east-1` region** must be available for the domain name. Default is no domain.                                                                                                                                                                                                                                                                                                                                                                                                                                                   | string |   `""`   |    no    |
| domain\_prefix                | Set this to specify a prefix to the domain name set via `domain_name`. Do not include a terminating period (.) as module will append that if this property is set. And if set we will look for a wildcard certificate **in `us-east-1` region**. Default is no prefix.                                                                                                                                                                                                                                                                                                                                                                                                                               | string |   `""`   |    no    |
| method\_types                 | List of API method integration types that will be deployed here.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |  list  |   n/a    |   yes    |
| opt\_cwlogging                | Set this attribute to 'yes' to include Cloud Watch logging of API gateway methods. This can be over-ridden per stage.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | string |  `"no"`  |    no    |
| opt\_cwmetrics                | Set this attribute to 'yes' to enable Cloud Watch Metrics by API gateway methods. This can be over-ridden per stage.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | string |  `"no"`  |    no    |
| opt\_throttling\_burst\_limit | Set this to a number to specify the maximum burst requests to allow. Default is 250. This can be over-ridden per stage.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | string | `"250"`  |    no    |
| opt\_throttling\_rate\_limit  | Set this to a number to specify the maximum number of requests per second to allow. Default is 1000. This can be over-ridden per stage.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | string | `"1000"` |    no    |
| stage\_vars                   | Optional map of stage variables available to all stages in this deployment.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |  map   | `<map>`  |    no    |
| stages                        | Specify names of all the deployment stages to define as part of this API as a map of stage-based objects,      where each key is a stage name and value is a map of option names and values.           The stages are processed after sorting the keys     in lexical order. You can use quoted numbers to preserve order and use `stage_name` option to specify the stage name.          Supported optional stage properties are: `stage_name`, `opt_cwlogging`, `opt_cwmetrics`, `opt_throttling_rate_limit`,     `opt_throttling_burst_limit`.<br><br>    E.g.      ```     stages = {       "0" = {         stage_name = "api"       }       "dev" {         opt_cwlogging = "yes"       }     } |  map   |   n/a    |   yes    |

**OUTPUTS**

| Name        | Description                                                                                                             |
| ----------- | ----------------------------------------------------------------------------------------------------------------------- |
| api\_urls   | The list of invocation URLs for for all the stages, including optional additional ones, using domain name if specified. |
| description | A description of the linked API methods associated with this deployment.                                                |
| raw\_urls   | The list of naked invocation URLs for for all the stages, including optional additional ones.                           |
| url         | The API URL, based on the custom domain name if specified. *Deprecated*                                                 |

### Module faaster/aws/api_lambda_auth
**INPUTS**

| Name           | Description                                                                                                                                                                                                                                              |  Type  | Default  | Required |
| -------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----: | :------: | :------: |
| api\_id        | The ID of the API as part of which this authorizer is deployed.                                                                                                                                                                                          | string |   n/a    |   yes    |
| auth\_ttl      | On successful authorization, specify the time-to-live for the cached authorization in seconds. Use 0 to disable caching (not recommended). Default is 300 (5 min).                                                                                       | string | `"300"`  |    no    |
| deploy\_prefix | A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only.                                                                                                                    | string |   n/a    |   yes    |
| env\_vars      | Optional map of environment variables to pass through to the Lambda function.                                                                                                                                                                            |  map   | `<map>`  |    no    |
| fn\_class      | This optional class name is embedded in the path to find the function code as follows: `functions/<class>/<fn_lang>/...`                                                                                                                                 | string |   `""`   |    no    |
| fn\_lang       | The language for the function, which can be one of the following: "node", "node10", "node8", "go", "go1". Unversioned language names map to highest version.                                                                                             | string | `"node"` |    no    |
| fn\_mem        | The amount of memory in MB for the Lambda function, in 64MB increments starting at 128MB. Defaults to 128.                                                                                                                                               | string | `"128"`  |    no    |
| fn\_name       | The name of the Lambda function, which is also the name of the folder with the Lambda handler. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder.                                       | string |   n/a    |   yes    |
| fn\_source     | Optional. Use this to specify the name of the folder with the Lambda handler if it's different from the name of the Lambda function. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder. | string |   `""`   |    no    |
| fn\_timeout    | The maximum time in seconds the function can run for. Default is 3s.                                                                                                                                                                                     | string |  `"3"`   |    no    |
| key\_arn       | The KMS key ARN for Lambda to use instead of the default service key provided by AWS.                                                                                                                                                                    | string |   `""`   |    no    |
| opt\_cwlogging | Set this attribute to 'yes' to include Cloud Watch logging access for the authorizer in the role.                                                                                                                                                        | string |  `"no"`  |    no    |

**OUTPUTS**

| Name      | Description |
| --------- | ----------- |
| auth\_id  |             |
| role\_arn |             |
| role\_id  |             |
| type      |             |
| version   |             |

### Module faaster/aws/api_lambda_auth_fn
**INPUTS**

| Name           | Description                                                                                                                                                                                                                                              |  Type  | Default  | Required |
| -------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----: | :------: | :------: |
| api\_id        | The ID of the API as part of which this authorizer is deployed.                                                                                                                                                                                          | string |   n/a    |   yes    |
| auth\_ttl      | On successful authorization, specify the time-to-live for the cached authorization in seconds. Use 0 to disable caching (not recommended). Default is 300 (5 min).                                                                                       | string | `"300"`  |    no    |
| deploy\_prefix | A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only.                                                                                                                    | string |   n/a    |   yes    |
| env\_vars      | Optional map of environment variables to pass through to the Lambda function.                                                                                                                                                                            |  map   | `<map>`  |    no    |
| fn\_class      | This optional class name is embedded in the path to find the function code as follows: `functions/<class>/<fn_lang>/...`                                                                                                                                 | string |   `""`   |    no    |
| fn\_lang       | The language for the function, which can be one of the following: "node", "node10", "node8", "go", "go1". Unversioned language names map to highest version.                                                                                             | string | `"node"` |    no    |
| fn\_mem        | The amount of memory in MB for the Lambda function, in 64MB increments starting at 128MB. Defaults to 128.                                                                                                                                               | string | `"128"`  |    no    |
| fn\_name       | The name of the Lambda function, which is also the name of the folder with the Lambda handler. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder.                                       | string |   n/a    |   yes    |
| fn\_source     | Optional. Use this to specify the name of the folder with the Lambda handler if it's different from the name of the Lambda function. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder. | string |   `""`   |    no    |
| fn\_timeout    | The maximum time in seconds the function can run for. Default is 3s.                                                                                                                                                                                     | string |  `"3"`   |    no    |
| key\_arn       | The KMS key ARN for Lambda to use instead of the default service key provided by AWS.                                                                                                                                                                    | string |   `""`   |    no    |
| lambda\_role   | The ARN for a lambda execution role. Use the `api_lambda_role` module to create one.                                                                                                                                                                     | string |   n/a    |   yes    |

**OUTPUTS**

| Name     | Description |
| -------- | ----------- |
| auth\_id |             |
| type     |             |
| version  |             |

### Module faaster/aws/api_lambda_method
**INPUTS**

| Name           | Description                                                                                                                                                                                                                                              |  Type  | Default  | Required |
| -------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----: | :------: | :------: |
| api\_id        | The ID of the API as part of which this method is deployed.                                                                                                                                                                                              | string |   n/a    |   yes    |
| auth\_id       | The custom authorizer ID; accessible from the `api_lambda_auth` module outputs. If not specified, no authorization is used.                                                                                                                              | string |   `""`   |    no    |
| deploy\_prefix | A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only.                                                                                                                    | string |   n/a    |   yes    |
| env\_vars      | Optional map of environment variables to pass through to the Lambda function.                                                                                                                                                                            |  map   | `<map>`  |    no    |
| fn\_class      | This optional class name is embedded in the path to find the function code as follows: `functions/<class>/<fn_lang>/...`                                                                                                                                 | string |   `""`   |    no    |
| fn\_lang       | The language for the function, which can be one of the following: "node", "node10", "node8", "go", "go1". Unversioned language names map to highest version.                                                                                             | string | `"node"` |    no    |
| fn\_mem        | The amount of memory in MB for the Lambda function, in 64MB increments starting at 128MB. Defaults to 128.                                                                                                                                               | string | `"128"`  |    no    |
| fn\_name       | The name of the Lambda function, which is also the name of the folder with the Lambda handler. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder.                                       | string |   n/a    |   yes    |
| fn\_source     | Optional. Use this to specify the name of the folder with the Lambda handler if it's different from the name of the Lambda function. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder. | string |   `""`   |    no    |
| fn\_timeout    | The maximum time in seconds the function can run for. Default is 3s.                                                                                                                                                                                     | string |  `"3"`   |    no    |
| http\_method   | The API method's HTTP method type. E.g. GET, POST etc.                                                                                                                                                                                                   | string |   n/a    |   yes    |
| key\_arn       | The KMS key ARN for Lambda to use instead of the default service key provided by AWS.                                                                                                                                                                    | string |   `""`   |    no    |
| opt\_cwlogging | Set this attribute to 'yes' to include Cloud Watch logging access for Lambda in the role.                                                                                                                                                                | string |  `"no"`  |    no    |
| resource\_id   | The ID of the API resource to which this method is associated.                                                                                                                                                                                           | string |   n/a    |   yes    |
| resource\_path | The full path of the API resource.                                                                                                                                                                                                                       | string |   n/a    |   yes    |

**OUTPUTS**

| Name      | Description |
| --------- | ----------- |
| role\_arn |             |
| role\_id  |             |
| type      |             |
| version   |             |

### Module faaster/aws/api_lambda_method_fn
**INPUTS**

| Name           | Description                                                                                                                                                                                                                                              |  Type  | Default  | Required |
| -------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----: | :------: | :------: |
| api\_id        | The ID of the API as part of which this method is deployed.                                                                                                                                                                                              | string |   n/a    |   yes    |
| auth\_id       | The custom authorizer ID; accessible from the `api_lambda_auth` module outputs. If not specified, no authorization is used.                                                                                                                              | string |   `""`   |    no    |
| deploy\_prefix | A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only.                                                                                                                    | string |   n/a    |   yes    |
| env\_vars      | Optional map of environment variables to pass through to the Lambda function.                                                                                                                                                                            |  map   | `<map>`  |    no    |
| fn\_class      | This optional class name is embedded in the path to find the function code as follows: `functions/<class>/<fn_lang>/...`                                                                                                                                 | string |   `""`   |    no    |
| fn\_lang       | The language for the function, which can be one of the following: "node", "node10", "node8", "go", "go1". Unversioned language names map to highest version.                                                                                             | string | `"node"` |    no    |
| fn\_mem        | The amount of memory in MB for the Lambda function, in 64MB increments starting at 128MB. Defaults to 128.                                                                                                                                               | string | `"128"`  |    no    |
| fn\_name       | The name of the Lambda function, which is also the name of the folder with the Lambda handler. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder.                                       | string |   n/a    |   yes    |
| fn\_source     | Optional. Use this to specify the name of the folder with the Lambda handler if it's different from the name of the Lambda function. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder. | string |   `""`   |    no    |
| fn\_timeout    | The maximum time in seconds the function can run for. Default is 3s.                                                                                                                                                                                     | string |  `"3"`   |    no    |
| http\_method   | The API method's HTTP method type. E.g. GET, POST etc.                                                                                                                                                                                                   | string |   n/a    |   yes    |
| key\_arn       | The KMS key ARN for Lambda to use instead of the default service key provided by AWS.                                                                                                                                                                    | string |   `""`   |    no    |
| lambda\_role   | The ARN for a lambda execution role. Use the `api_lambda_role` module to create one.                                                                                                                                                                     | string |   n/a    |   yes    |
| resource\_id   | The ID of the API resource to which this method is associated.                                                                                                                                                                                           | string |   n/a    |   yes    |
| resource\_path | The full path of the API resource.                                                                                                                                                                                                                       | string |   n/a    |   yes    |

**OUTPUTS**

| Name    | Description |
| ------- | ----------- |
| type    |             |
| version |             |

### Module faaster/aws/api_lambda_role
**INPUTS**

| Name           | Description                                                                                                                           |  Type  | Default | Required |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------- | :----: | :-----: | :------: |
| deploy\_prefix | A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only. | string |   n/a   |   yes    |
| name           | Unique name for the role. When defining multiple lambda execution roles, make the names diatinctive to help with troubleshooting.     | string |   n/a   |   yes    |
| opt\_cwlogging | Set this attribute to 'yes' to include Cloud Watch logging access for Lambda in the role.                                             | string | `"no"`  |    no    |

**OUTPUTS**

| Name      | Description |
| --------- | ----------- |
| role\_arn |             |
| role\_id  |             |

### Module faaster/aws/api_resource
**INPUTS**

| Name       | Description                                                                                                                                            |  Type  | Default | Required |
| ---------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | :----: | :-----: | :------: |
| api\_id    | The ID of the API Gateway REST API under which this path is deployed.                                                                                  | string |   n/a   |   yes    |
| parent\_id | The ID of the resource that will be the parent path under which this path is deployed. Can be the `root_id` of an API or the `id` of another resource. | string |   n/a   |   yes    |
| path\_name | The name of the API path part                                                                                                                          | string |   n/a   |   yes    |

**OUTPUTS**

| Name | Description |
| ---- | ----------- |
| id   |             |
| path |             |

### Module faaster/aws/api_stages_lambda_method
**INPUTS**

| Name             | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |  Type  | Default  | Required |
| ---------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----: | :------: | :------: |
| api\_id          | The ID of the API as part of which this method is deployed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | string |   n/a    |   yes    |
| auth\_id         | The custom authorizer ID; accessible from the `api_lambda_auth` module outputs. If not specified, no authorization is used.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | string |   `""`   |    no    |
| deploy\_prefix   | A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | string |   n/a    |   yes    |
| env\_vars        | Optional map of environment variables to pass through to the Lambda function.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |  map   | `<map>`  |    no    |
| fn\_lang         | The language for the function, which can be one of the following: "node", "node10", "node8", "go", "go1". Unversioned language names map to highest version.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | string | `"node"` |    no    |
| fn\_mem          | The amount of memory in MB for the Lambda function, in 64MB increments starting at 128MB. Defaults to 128.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | string | `"128"`  |    no    |
| fn\_name\_prefix | The name prefix of the Lambda functions, which will be combined with the stage name as suffix to define a Lambda function per stage in the list of stage names.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | string |   n/a    |   yes    |
| fn\_stages       | Specify names of all the deployment stages for which to define Lambda functions as part of this multi-stage     API method, where each key is a stage name and value is a map of optional config for the per-stage Lambda overriding     any global config.           The stages are processed after sorting the keys in lexical order. You can use quoted numbers to preserve order and      use `stage_name` option to specify the stage name.<br><br>    Supported optional config properties are: `stage_name`, `fn_mem`, `fm_timeout`, `fn_lang`.<br><br>    E.g.      ```     fn_stages = {       "0" = {         stage_name = "api"         fn_mem = 128       }       "dev" {         fn_lang = "go"       }     } |  map   |   n/a    |   yes    |
| fn\_timeout      | The maximum time in seconds the function can run for. Default is 3s.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | string |  `"3"`   |    no    |
| http\_method     | The API method's HTTP method type. E.g. GET, POST etc.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | string |   n/a    |   yes    |
| key\_arn         | The KMS key ARN for Lambda to use instead of the default service key provided by AWS.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | string |   `""`   |    no    |
| opt\_cwlogging   | Set this attribute to 'no' to exclude Cloud Watch logging access for Lambda in the role. Enabled by default.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | string | `"yes"`  |    no    |
| resource\_id     | The ID of the API resource to which this method is associated.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | string |   n/a    |   yes    |
| resource\_path   | The full path of the API resource.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | string |   n/a    |   yes    |

**OUTPUTS**

| Name               | Description                                    |
| ------------------ | ---------------------------------------------- |
| function\_arns     | The list of per-stage Lambda function ARN's    |
| function\_names    | The list of per-stage Lambda function names    |
| function\_versions | The list of per-stage Lambda function versions |
| role\_arn          |                                                |
| role\_id           |                                                |
| type               |                                                |

### Module faaster/aws/api_stages_lambda_method_fn
**INPUTS**

| Name             | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |  Type  | Default  | Required |
| ---------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----: | :------: | :------: |
| api\_id          | The ID of the API as part of which this method is deployed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | string |   n/a    |   yes    |
| auth\_id         | The custom authorizer ID; accessible from the `api_lambda_auth` module outputs. If not specified, no authorization is used.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | string |   `""`   |    no    |
| deploy\_prefix   | A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | string |   n/a    |   yes    |
| env\_vars        | Optional map of environment variables to pass through to the Lambda function.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |  map   | `<map>`  |    no    |
| fn\_lang         | The language for the function, which can be one of the following: "node", "node10", "node8", "go", "go1". Unversioned language names map to highest version.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | string | `"node"` |    no    |
| fn\_mem          | The amount of memory in MB for the Lambda function, in 64MB increments starting at 128MB. Defaults to 128.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | string | `"128"`  |    no    |
| fn\_name\_prefix | The name prefix of the Lambda functions, which will be combined with the stage name as suffix to define a Lambda function per stage in the list of stage names.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | string |   n/a    |   yes    |
| fn\_stages       | Specify names of all the deployment stages for which to define Lambda functions as part of this multi-stage     API method, where each key is a stage name and value is a map of optional config for the per-stage Lambda overriding     any global config.           The stages are processed after sorting the keys in lexical order. You can use quoted numbers to preserve order and      use `stage_name` option to specify the stage name.<br><br>    Supported optional config properties are: `stage_name`, `fn_mem`, `fm_timeout`, `fn_lang`.<br><br>    E.g.      ```     fn_stages = {       "0" = {         stage_name = "api"         fn_mem = 128       }       "dev" {         fn_lang = "go"       }     } |  map   |   n/a    |   yes    |
| fn\_timeout      | The maximum time in seconds the function can run for. Default is 3s.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | string |  `"3"`   |    no    |
| http\_method     | The API method's HTTP method type. E.g. GET, POST etc.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | string |   n/a    |   yes    |
| key\_arn         | The KMS key ARN for Lambda to use instead of the default service key provided by AWS.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | string |   `""`   |    no    |
| lambda\_role     | The ARN for a lambda execution role. Use the `api_lambda_role` module to create one.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | string |   n/a    |   yes    |
| resource\_id     | The ID of the API resource to which this method is associated.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | string |   n/a    |   yes    |
| resource\_path   | The full path of the API resource.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | string |   n/a    |   yes    |

**OUTPUTS**

| Name               | Description                                    |
| ------------------ | ---------------------------------------------- |
| function\_arns     | The list of per-stage Lambda function ARN's    |
| function\_names    | The list of per-stage Lambda function names    |
| function\_versions | The list of per-stage Lambda function versions |
| type               |                                                |

### Module faaster/aws/cron_lambda_fn
**INPUTS**

| Name           | Description                                                                                                                                                                                                                                              |  Type  | Default  | Required |
| -------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----: | :------: | :------: |
| deploy\_prefix | A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only.                                                                                                                    | string |   n/a    |   yes    |
| env\_vars      | Optional map of environment variables to pass through to the Lambda function.                                                                                                                                                                            |  map   | `<map>`  |    no    |
| fn\_class      | This optional class name is embedded in the path to find the function code as follows: `functions/<class>/<fn_lang>/...`                                                                                                                                 | string |   `""`   |    no    |
| fn\_lang       | The language for the function, which can be one of the following: "node", "node10", "node8", "go", "go1". Unversioned language names map to highest version.                                                                                             | string | `"node"` |    no    |
| fn\_mem        | The amount of memory in MB for the Lambda function, in 64MB increments starting at 128MB. Defaults to 128.                                                                                                                                               | string | `"128"`  |    no    |
| fn\_name       | The name of the Lambda function, which is also the name of the folder with the Lambda handler. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder.                                       | string |   n/a    |   yes    |
| fn\_source     | Optional. Use this to specify the name of the folder with the Lambda handler if it's different from the name of the Lambda function. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder. | string |   `""`   |    no    |
| fn\_timeout    | The maximum time in seconds the function can run for. Default is 3s.                                                                                                                                                                                     | string |  `"3"`   |    no    |
| key\_arn       | The KMS key ARN for Lambda to use instead of the default service key provided by AWS.                                                                                                                                                                    | string |   `""`   |    no    |
| opt\_cwlogging | Set this attribute to 'yes' to include Cloud Watch logging access for Lambda in the role.                                                                                                                                                                | string |  `"no"`  |    no    |
| sched\_expr    | The scheduling expression. For example, cron(0 20 * * ? *) or rate(5 minutes). See [AWS docs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html) for details.                                                              | string |   n/a    |   yes    |

**OUTPUTS**

| Name        | Description |
| ----------- | ----------- |
| fn\_version |             |
| role\_arn   |             |
| role\_id    |             |

### Module faaster/aws/crud_dynamo_access
**INPUTS**

| Name                 | Description                                                                           |  Type  | Default | Required |
| -------------------- | ------------------------------------------------------------------------------------- | :----: | :-----: | :------: |
| delete\_allow\_count | The no. of Lambda role IDs to attach to the delete access policy. Use 0 if none.      | string |   n/a   |   yes    |
| delete\_allow\_roles | The Lambda role IDs to attach to the delete access policy. Use an empty list if none. |  list  |   n/a   |   yes    |
| read\_allow\_count   | The no. of Lambda role IDs to attach to the read access policy. Use 0 if none.        | string |   n/a   |   yes    |
| read\_allow\_roles   | The Lambda role IDs to attach to the read access policy. Use an empty list if none.   |  list  |   n/a   |   yes    |
| table\_arn           | The ARN of the DB table associated with these access policies and attachment.         | string |   n/a   |   yes    |
| table\_name          | The name of the DB table which is usedwithin the policy names.                        | string |   n/a   |   yes    |
| write\_allow\_count  | The no. of Lambda role IDs to attach to the write access policy. Use 0 if none.       | string |   n/a   |   yes    |
| write\_allow\_roles  | The Lambda role IDs to attach to the write access policy. Use an empty list if none.  |  list  |   n/a   |   yes    |

### Module faaster/aws/crud_es_access
**INPUTS**

| Name                 | Description                                                                                           |  Type  | Default | Required |
| -------------------- | ----------------------------------------------------------------------------------------------------- | :----: | :-----: | :------: |
| delete\_allow\_count | The no. of Lambda role IDs to attach to the delete access policy. Use 0 if none.                      | string |   n/a   |   yes    |
| delete\_allow\_roles | The Lambda role IDs to attach to the delete access policy. Use an empty list if none.                 |  list  |   n/a   |   yes    |
| elasticsearch\_arn   | The ARN of the ElasticSearch domain associated with these access policies and attachment.             | string |   n/a   |   yes    |
| elasticsearch\_index | The index within the ElasticSearch domain to which to limit this access policy. Default to any index. | string |  `""`   |    no    |
| elasticsearch\_name  | The name of the ElasticSearch domain which is used within the policy names.                           | string |   n/a   |   yes    |
| read\_allow\_count   | The no. of Lambda role IDs to attach to the read access policy. Use 0 if none.                        | string |   n/a   |   yes    |
| read\_allow\_roles   | The Lambda role IDs to attach to the read access policy. Use an empty list if none.                   |  list  |   n/a   |   yes    |
| write\_allow\_count  | The no. of Lambda role IDs to attach to the write access policy. Use 0 if none.                       | string |   n/a   |   yes    |
| write\_allow\_roles  | The Lambda role IDs to attach to the write access policy. Use an empty list if none.                  |  list  |   n/a   |   yes    |

### Module faaster/aws/crud_s3_access
**INPUTS**

| Name                 | Description                                                                                             |  Type  | Default | Required |
| -------------------- | ------------------------------------------------------------------------------------------------------- | :----: | :-----: | :------: |
| bucket\_arn          | The ARN of the S3 bucket associated with these access policies and attachment.                          | string |   n/a   |   yes    |
| bucket\_name         | The name of the S3 bucket which is used within the policy names.                                        | string |   n/a   |   yes    |
| bucket\_prefix       | The path prefix within the S3 bucket to which to limit this access policy. Do not specify starting `/`. | string |  `""`   |    no    |
| delete\_allow\_count | The no. of Lambda role IDs to attach to the delete access policy. Use 0 if none.                        | string |   n/a   |   yes    |
| delete\_allow\_roles | The Lambda role IDs to attach to the delete access policy. Use an empty list if none.                   |  list  |   n/a   |   yes    |
| read\_allow\_count   | The no. of Lambda role IDs to attach to the read access policy. Use 0 if none.                          | string |   n/a   |   yes    |
| read\_allow\_roles   | The Lambda role IDs to attach to the read access policy. Use an empty list if none.                     |  list  |   n/a   |   yes    |
| write\_allow\_count  | The no. of Lambda role IDs to attach to the write access policy. Use 0 if none.                         | string |   n/a   |   yes    |
| write\_allow\_roles  | The Lambda role IDs to attach to the write access policy. Use an empty list if none.                    |  list  |   n/a   |   yes    |

### Module faaster/aws/lambda_fn
**INPUTS**

| Name           | Description                                                                                                                                                                                                                                              |  Type  | Default  | Required |
| -------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----: | :------: | :------: |
| deploy\_prefix | A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only.                                                                                                                    | string |   n/a    |   yes    |
| env\_vars      | Optional map of environment variables to pass through to the Lambda function.                                                                                                                                                                            |  map   | `<map>`  |    no    |
| fn\_class      | This optional class name is embedded in the path to find the function code as follows: `functions/<class>/<fn_lang>/...`                                                                                                                                 | string |   `""`   |    no    |
| fn\_lang       | The language for the function, which can be one of the following: "node" (default), "node10", "node8", "go", "go1". Unversioned language names map to highest version.                                                                                   | string | `"node"` |    no    |
| fn\_mem        | The amount of memory in MB for the Lambda function, in 64MB increments starting at 128MB. Defaults to 128.                                                                                                                                               | string | `"128"`  |    no    |
| fn\_name       | The name of the Lambda function, which is also the name of the folder with the Lambda handler. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder.                                       | string |   n/a    |   yes    |
| fn\_source     | Optional. Use this to specify the name of the folder with the Lambda handler if it's different from the name of the Lambda function. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder. | string |   `""`   |    no    |
| fn\_timeout    | The maximum time in seconds the function can run for. Default is 3s.                                                                                                                                                                                     | string |  `"3"`   |    no    |
| key\_arn       | The KMS key ARN for Lambda to use instead of the default service key provided by AWS.                                                                                                                                                                    | string |   `""`   |    no    |
| lambda\_role   | The ARN for a lambda execution role. Use the `api_lambda_role` module to create one.                                                                                                                                                                     | string |   n/a    |   yes    |

**OUTPUTS**

| Name               | Description |
| ------------------ | ----------- |
| arn                |             |
| invoke\_arn        |             |
| last\_modified     |             |
| source\_code\_hash |             |
| source\_code\_size |             |
| version            |             |

