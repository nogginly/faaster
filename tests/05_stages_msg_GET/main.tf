provider "aws" {
  region = var.aws_region
}

provider "archive" {
}

module "test_api" {
  source        = "./modules/faaster/aws/api"
  api_name      = "Test 05 API"
  api_desc      = "Test API for testing multi-stage API deployment and method modules"
  deploy_prefix = var.deploy
}

module "test_message_base" {
  source    = "./modules/faaster/aws/api_resource"
  path_name = "message"
  api_id    = module.test_api.id
  parent_id = module.test_api.root_id
}

module "test_message_by_id" {
  source = "./modules/faaster/aws/api_resource"

  # The use of `{xxx}` resource allows us to handle a parameter in the request "route"
  # E.g. /message/{id}
  path_name = "{id}"

  api_id    = module.test_api.id
  parent_id = module.test_message_base.id
}

module "test_lambda_role" {
  source        = "./modules/faaster/aws/api_lambda_role"
  name          = "test_api_role"
  opt_cwlogging = "yes"
  deploy_prefix = var.deploy
}

locals {
  stages = {
    "0" = {
      stage_name    = "v1"
      opt_cwmetrics = "yes"
    }
    "1" = {
      stage_name = "dev"
    }
    "8-multi-stage" = {}
  }

  stage_names = ["v1", "dev", "8-multi-stage"]
}

module "test_message_GET" {
  # Test the `api_stages_lambda_method_fn` module with externally defined IAM role.
  #
  # GET /STAGE/message/KEY
  #
  source = "./modules/faaster/aws/api_stages_lambda_method_fn"

  fn_stages = {
    "0" = {
      stage_name = "v1"
    }
    "1" = {
      stage_name = "dev"
    }
    "8-multi-stage" = {
      fn_lang = "go"
    }
  }

  fn_name_prefix = "message_GET"
  http_method    = "GET"
  api_id         = module.test_api.id
  resource_id    = module.test_message_by_id.id
  resource_path  = module.test_message_by_id.path
  lambda_role    = module.test_lambda_role.role_arn
  deploy_prefix  = var.deploy
}

module "test_message_POST" {
  # Test the `api_stages_lambda_method` module with a self-contained IAM role.
  #
  # POST /STAGE/message
  #
  source = "./modules/faaster/aws/api_stages_lambda_method_fn"

  fn_stages = {
    "0" = {
      stage_name = "v1"
    }
    "1" = {
      stage_name = "dev"
    }
    "8-multi-stage" = {}
  }

  fn_name_prefix = "message_POST"
  http_method    = "POST"
  api_id         = module.test_api.id
  resource_id    = module.test_message_base.id
  resource_path  = module.test_message_base.path
  lambda_role    = module.test_lambda_role.role_arn
  deploy_prefix  = var.deploy
}

module "test_deploy_prod" {
  source        = "./modules/faaster/aws/api_deploy"
  stages        = local.stages
  api_id        = module.test_api.id
  deploy_prefix = var.deploy
  domain_name   = var.base_domain_name
  domain_prefix = var.deploy
  opt_cwlogging = "yes"

  stage_vars = {
    myStageVar = "MyVeryOwnStageVar"
  }

  #
  # This is used to explicitly specify a dependency to the methods because
  # otherwise the deployment gets created prematurely and fails.
  method_types = [
    module.test_message_GET.type,
    module.test_message_POST.type,
  ]
  #
  # Don't forget to taint the following deployment resources if/when adding/removing
  # methods and stages in this deployment. Use the following script:
  #
  #   bin/faastaint_deployment TARGET DEPLOYMODULE NUMSTAGES
  #
}

output "api_urls" {
  value = module.test_deploy_prod.api_urls
}

output "api_list" {
  value = module.test_deploy_prod.description
}

output "api_raw_urls" {
  value = module.test_deploy_prod.raw_urls
}

output "api_functions" {
  value = {
    names = concat(
      module.test_message_GET.function_names,
      module.test_message_POST.function_names,
    )
    arns = concat(
      module.test_message_GET.function_arns,
      module.test_message_POST.function_arns,
    )
  }
}

