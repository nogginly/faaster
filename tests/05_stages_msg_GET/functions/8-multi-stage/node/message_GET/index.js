const DEPLOY_STAGE = process.env.DEPLOY_STAGE;

exports.handler = function(event, context, callback) {
    callback(null, {
        statusCode: "200",
        body: JSON.stringify({
            message: "Branched Hello World from " + DEPLOY_STAGE,
            event: event,
            ctx: context,
            env: process.env,
        }),
        headers: {
            "Content-Type": "application/json",
        },
    });
};
