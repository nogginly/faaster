provider "aws" {
  region = var.aws_region
}

provider "archive" {
}

module "example_api" {
  source        = "./modules/faaster/aws/api"
  api_name      = "Example API"
  api_desc      = "Example API for testing Lambda and API gateway custom authorizer"
  deploy_prefix = var.deploy
}

module "example_message_base" {
  source    = "./modules/faaster/aws/api_resource"
  path_name = "message"
  api_id    = module.example_api.id
  parent_id = module.example_api.root_id
}

module "example_message_by_id" {
  source = "./modules/faaster/aws/api_resource"

  # The use of `{xxx}` resource allows us to handle a parameter in the request "route"
  # E.g. /message/{id}
  path_name = "{id}"

  api_id    = module.example_api.id
  parent_id = module.example_message_base.id
}

module "ex_message_AUTH" {
  source        = "./modules/faaster/aws/api_lambda_auth"
  fn_class      = "testclass"
  fn_name       = "message_AUTH"
  api_id        = module.example_api.id
  opt_cwlogging = "yes"
  deploy_prefix = var.deploy
  auth_ttl      = 30
}

module "ex_message_GET" {
  source        = "./modules/faaster/aws/api_lambda_method"
  fn_name       = "message_GET"
  http_method   = "GET"
  api_id        = module.example_api.id
  resource_id   = module.example_message_by_id.id
  resource_path = module.example_message_by_id.path
  opt_cwlogging = "yes"
  deploy_prefix = var.deploy
  auth_id       = module.ex_message_AUTH.auth_id
}

module "example_deploy_prod" {
  source = "./modules/faaster/aws/api_deploy"

  stages = {
    "api" = {}
  }

  api_id        = module.example_api.id
  deploy_prefix = var.deploy
  domain_name   = var.base_domain_name
  domain_prefix = var.deploy

  #
  # This is used to explicitly specify a dependency to the methods because
  # otherwise the deployment gets created prematurely and fails.
  method_types = [
    module.ex_message_AUTH.type,
    module.ex_message_GET.type,
  ]
}

output "api_url" {
  value = module.example_deploy_prod.url
}

output "api_list" {
  value = module.example_deploy_prod.description
}

