/*
* Copyright 2015-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
*
*     http://aws.amazon.com/apache2.0/
*
* or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/

const AwsApiAuthPolicy = require("./ApiAuthPolicy.js");

const userDB = {
    moses: {
        password: "tablets",
        allow: {
            method: "GET",
            path: "/message/*"
        }
    },
    rameses: {
        password: "pyramids",
        allow: {
            method: "GET",
            path: "/message/*"
        }
    },
    siddhartha: {
        password: "enlightenment",
        allow: {
            method: "GET",
            path: "/message/*"
        }
    },
    krishna: {
        password: "papyrus",
        allow: {
            method: "POST",
            path: "/message/*"
        }
    }
};

const validateAuthRE = /\s*(\w+)\s*\|\s*(\w+)/;

//
// Simple "user|password" token validation
function validateAuth(token) {
    var creds = token.match(validateAuthRE);
    console.log("RE creds: " + JSON.stringify(creds));
    if (creds) {
        var dbUser = userDB[creds[1]];
        if (dbUser && dbUser.password == creds[2]) {
            return {
                user: creds[1],
                allow: dbUser.allow
            };
        }
    }
    return null; // invalid
}

exports.handler = function(event, context, callback) {
    console.log("Client token: " + event.authorizationToken);
    console.log("Method ARN: " + event.methodArn);

    // validate the incoming token
    // and produce the principal user identifier associated with the token

    // this could be accomplished in a number of ways:
    // 1. Call out to OAuth provider
    // 2. Decode a JWT token inline
    // 3. Lookup in a self-managed DB

    var auth = validateAuth(event.authorizationToken);
    console.log("Validated Auth: " + JSON.stringify(auth));

    // you can send a 401 Unauthorized response to the client by failing like so:
    // callback("Unauthorized", null);

    if (!auth) {
        callback("Unauthorized", null);
        return;
    }

    // if the token is valid, a policy must be generated which will allow or deny access to the client

    // if access is denied, the client will receive a 403 Access Denied response
    // if access is allowed, API Gateway will proceed with the backend integration configured on the method that was called

    // this function must generate a policy that is associated with the recognized principal user identifier.
    // depending on your use case, you might store policies in a DB, or generate them on the fly

    // keep in mind, the policy is cached for 5 minutes by default (TTL is configurable in the authorizer)
    // and will apply to subsequent calls to any method/resource in the RestApi
    // made with the same token

    // finally, build the policy
    const opts = AwsApiAuthPolicy.parseMethodARN(event.methodArn);
    var authResp = new AwsApiAuthPolicy(auth.user, opts.awsAccountId, opts)
        .allowMethod(auth.allow.method, auth.allow.path)
        .build();

    console.log("Auth Policy" + JSON.stringify(authResp));

    // new! -- add additional key-value pairs
    // these are made available by APIGW like so: $context.authorizer.<key>
    // additional context is cached
    authResp.context = {
        key: "value", // $context.authorizer.key -> value
        number: 1,
        bool: true
    };
    // authResponse.context.arr = ['foo']; <- this is invalid, APIGW will not accept it
    // authResponse.context.obj = {'foo':'bar'}; <- also invalid

    callback(null, authResp);
};
