provider "aws" {
  region = var.aws_region
}

provider "archive" {
}

module "example_api" {
  source        = "./modules/faaster/aws/api"
  api_name      = "Example API"
  api_desc      = "Example API for testing Lambda and API gateway setup using Terraform"
  deploy_prefix = var.deploy
}

module "example_message_base" {
  source    = "./modules/faaster/aws/api_resource"
  path_name = "message"
  api_id    = module.example_api.id
  parent_id = module.example_api.root_id
}

module "example_message_by_id" {
  source = "./modules/faaster/aws/api_resource"

  # The use of `{xxx}` resource allows us to handle a parameter in the request "route"
  # E.g. /message/{id}
  path_name = "{id}"

  api_id    = module.example_api.id
  parent_id = module.example_message_base.id
}

module "ex_lambda_role" {
  source        = "./modules/faaster/aws/api_lambda_role"
  name          = "ex_api_role"
  opt_cwlogging = "yes"
  deploy_prefix = var.deploy
}

module "ex_message_POST" {
  source        = "./modules/faaster/aws/api_lambda_method_fn"
  fn_name       = "message_POST"
  fn_lang       = "node8"
  http_method   = "POST"
  api_id        = module.example_api.id
  resource_id   = module.example_message_base.id
  resource_path = module.example_message_base.path
  lambda_role   = module.ex_lambda_role.role_arn
  deploy_prefix = var.deploy
}

module "ex_message_GET" {
  source        = "./modules/faaster/aws/api_lambda_method_fn"
  fn_name       = "message_GET"
  http_method   = "GET"
  api_id        = module.example_api.id
  resource_id   = module.example_message_by_id.id
  resource_path = module.example_message_by_id.path
  lambda_role   = module.ex_lambda_role.role_arn
  deploy_prefix = var.deploy
}

module "example_deploy_prod" {
  source = "./modules/faaster/aws/api_deploy"

  stages = {
    "api" = {}
    "dev" = {}
  }

  api_id        = module.example_api.id
  deploy_prefix = var.deploy
  domain_name   = var.base_domain_name
  domain_prefix = var.deploy

  stage_vars = {
    myStageVar = "MyVeryOwnStageVar"
  }

  #
  # This is used to explicitly specify a dependency to the methods because
  # otherwise the deployment gets created prematurely and fails.
  method_types = [
    module.ex_message_POST.type,
    module.ex_message_GET.type,
  ]
}

output "api_urls" {
  value = module.example_deploy_prod.api_urls
}

output "api_list" {
  value = module.example_deploy_prod.description
}

output "api_raw_urls" {
  value = module.example_deploy_prod.raw_urls
}

