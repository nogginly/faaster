exports.handler = function(event, context, callback) {
    callback(null, {
      statusCode: '200',
      body: JSON.stringify({ 
        'message': 'hello world',
        'event' : event,
        'ctx' : context
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
  };
  