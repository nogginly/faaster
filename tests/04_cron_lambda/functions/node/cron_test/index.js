"use strict";

const TEST_NAME = process.env.TEST_NAME;

exports.handler = (event, context, callback) => {
    console.log("Faaster `cron_lambda_fn` test: %s", TEST_NAME);
    console.log("Received event:", JSON.stringify(event, null, 2));
    callback(null, "Finished");
};
