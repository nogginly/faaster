provider "aws" {
  region = var.aws_region
}

provider "archive" {
}

module "ex_cron_test" {
  source        = "./modules/faaster/aws/cron_lambda_fn"
  fn_name       = "cron_test"
  fn_lang       = "node"
  fn_timeout    = 4
  fn_mem        = 128
  opt_cwlogging = "yes"
  deploy_prefix = var.deploy
  sched_expr    = "rate(5 minutes)"

  env_vars = {
    TEST_NAME = "cron_test"
  }
}

module "ex_cron_test_2" {
  source        = "./modules/faaster/aws/cron_lambda_fn"
  fn_name       = "cron_test_2"
  fn_source     = "cron_test" # Use same source as other lambda.
  fn_lang       = "node"
  fn_timeout    = 4
  fn_mem        = 128
  opt_cwlogging = "yes"
  deploy_prefix = var.deploy
  sched_expr    = "cron(0/5 * * * ? *)"

  env_vars = {
    TEST_NAME = "cron_test_2"
  }
}

