#!/bin/bash

# Graphviz DOT diagrams

export DOTCMD="dot"
export DOTPNG_OPTS="-Tpng -Gdpi=200"
export DOTSVG_OPTS="-Tsvg"

export DOTFILES=`echo diagrams/*.dot`

for F in $DOTFILES
do
    echo Processing $F
    $RUN dot $DOTPNG_OPTS $F -o${F%.*}.png
    $RUN dot $DOTSVG_OPTS $F -o${F%.*}.svg
done

# Mermaid diagrams

export MMDCCMD="mmdc"

export MMDCFILES=`echo diagrams/*.mermaid`
export MMDC_OPTS="-b transparent -t neutral"

for F in $MMDCFILES
do
    echo Processing $F
    $MMDCCMD $MMDC_OPTS -i $F -o ${F%.*}.png 
done

echo Done.
