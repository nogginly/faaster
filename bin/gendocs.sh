#!/bin/bash

#
# Find each module and run it through 'terraform-docs' to generate one big document with documentation
# for the modules.
#
ls ./modules/aws/ | xargs -I % bash -c "echo \"# Module faaster/aws/%\" ; terraform-docs md ./modules/aws/%/"
