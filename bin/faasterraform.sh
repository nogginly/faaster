#!/usr/bin/env bash
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

x=`terraform version`
if [[ $x =~ Terraform\ v[0-9]+\.([0-9]+)\. ]]
then
    tfver_minor=${BASH_REMATCH[1]}
    if [[ ${tfver_minor} -ge 12 ]]
    then
        echo Terraform Version \>= 0.12.x
        cmd="${0%/*}/faasterraform.12.sh"
    else
        echo Terraform Version \<= 0.11.x
        cmd="${0%/*}/faasterraform.pre12.sh"
    fi
    # Execute the terraform version-specific faaster wrapper 
    $cmd $@
else
    echo Failed to identify terraform version.
    exit -1
fi