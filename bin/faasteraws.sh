#!/usr/bin/env bash

NAMESPACE=deployments

HELP="

Usage: ${0} TARGET AWSSVC AWSCMD [AWSOPTIONS...]

    TARGET - name of configuration target in your $NAMESPACE/ folder
             with the 'credentials' file used to source in env vars. Required.
    AWSSVC - The 'aws' service name per the AWS CLI reference
    AWSCMD - The service-specific sub-command, followed by its options etc.

"

# always want JSON output.
export AWS_DEFAULT_OUTPUT=json

if [ -f "$NAMESPACE/$1/credentials" ]; then
    source "$NAMESPACE/$1/credentials"
    case $2 in
    "")
        echo "ERR: AWS command required."
        echo "$HELP"
        ;;
    *)
        shift       # shift args by 1
        aws "$@"
        ;;
    esac
else
    echo "ERR: Specify name of configuration target in your $NAMESPACE/ folder."
    echo "$HELP"
fi

