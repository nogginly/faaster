#!/usr/bin/env bash
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

# The help output
#
HELP="

Usage: ${0} WHERE

    WHERE - Path into which this script sets up a faaster-based app project.

"

WHEREPATH=$1

# check that script was called with a parameter
if [ "$WHEREPATH" = "" ]; then
    echo Missing WHERE path parameter required.
    exit -1
fi

# if specified path doesn't exist try to make it
if [ ! -d "$WHEREPATH" ]; then
    mkdir -p $WHEREPATH
fi

# if specified path is valid then set it up/
if [ -d "$WHEREPATH" ]; then
    # path exists
    cd $WHEREPATH

    # initialize it as a new git repo
    git init

    # make a tmp directory into which to clone the "faaster-example" repo
    mkdir tmp
    cd tmp
    git clone https://gitlab.com/nogginly/faaster-example.git
    cd faaster-example

    # copy the bits we want from the example repo
    cp .gitignore *.tf ../../
    cp -R functions ../../
    cd ../../

    # cleanup the tmp directory
    rm -rf tmp

    # make the template folders for the app
    mkdir deps modules bin deployments

    # add "faaster" as a submodule
    git submodule add https://gitlab.com/nogginly/faaster.git deps/faaster
    cd modules

    # make faaster modules available under "modules" per terraform convention
    ln -s ../deps/faaster/modules faaster
    cd ../bin

    # make faaster scripts available under "bin" 
    ln -s ../deps/faaster/bin/faasterraform.sh
    ln -s ../deps/faaster/bin/faasteraws.sh
    ln -s ../deps/faaster/bin/taint_deployment.sh

    # ready to go
    echo
    echo ---------
    echo Your faaster-based app is ready to use at "$WHEREPATH"
    echo ---------
    echo
else
    echo Not a valid directory: $WHEREPATH
    exit -1
fi



