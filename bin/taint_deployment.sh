#!/usr/bin/env bash
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

# The help output
#
HELP="

Usage: ${0} TARGET DEPLOYMODULE NUMSTAGES

    TARGET - name of configuration target in your $NAMESPACE/ folder
             with the 'credentials' file used to source in env vars. Required.
    DEPLOYMODULE - The name of the API deployment module
    NUMSTAGES - The total number of stages

"

#RUN=echo      # Uncomment when debugging script
BINDIR="${0%/*}"
FTPATH="$BINDIR/faasterraform.sh"

TARGET=$1
DEPLOYMODULE=$2
NUMSTAGES=$3

if [ "$TARGET" = "" ]; then
    echo Missing TARGET
    exit -1
fi

if [ "$DEPLOYMODULE" = "" ]; then
    echo Missing DEPLOYMODULE
    exit -1
fi

if [ "$NUMSTAGES" = "" ]; then
    echo Missing NUMSTAGES
    exit -1
fi

$RUN $FTPATH $TARGET taint -module=$DEPLOYMODULE aws_api_gateway_deployment.deploy
$RUN $FTPATH $TARGET taint -module=$DEPLOYMODULE aws_api_gateway_base_path_mapping.deploy

for ((i=0;i<NUMSTAGES-1;i++)); do
    $RUN $FTPATH $TARGET taint -module=$DEPLOYMODULE aws_api_gateway_stage.deploy_add_stages.$i
    $RUN $FTPATH $TARGET taint -module=$DEPLOYMODULE aws_api_gateway_base_path_mapping.deploy_add_stages.$i
done
