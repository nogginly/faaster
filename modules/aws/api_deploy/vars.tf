variable "method_types" {
  type        = list(string)
  description = "List of API method integration types that will be deployed here."
}

variable "api_id" {
  description = "The ID of the API Gateway REST API that is deployed."
}

variable "stages" {
  type = map(map(string))

  description = <<DOC
    Specify names of all the deployment stages to define as part of this API as a map of stage-based objects, 
    where each key is a stage name and value is a map of option names and values. 
    
    The stages are processed after sorting the keys
    in lexical order. You can use quoted numbers to preserve order and use `stage_name` option to specify the stage name.
    
    Supported optional stage properties are: `stage_name`, `opt_cwlogging`, `opt_cwmetrics`, `opt_throttling_rate_limit`,
    `opt_throttling_burst_limit`.

    E.g. 
    ```
    stages = {
      "0" = {
        stage_name = "api"
      }
      "dev" {
        opt_cwlogging = "yes"
      }
    }
    
DOC

}

variable "deploy_prefix" {
  description = "A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only."
}

variable "opt_cwlogging" {
  description = "Set this attribute to 'yes' to include Cloud Watch logging of API gateway methods. This can be over-ridden per stage."
  default     = "no"
}

variable "opt_cwmetrics" {
  description = "Set this attribute to 'yes' to enable Cloud Watch Metrics by API gateway methods. This can be over-ridden per stage."
  default     = "no"
}

variable "opt_throttling_rate_limit" {
  description = "Set this to a number to specify the maximum number of requests per second to allow. Default is 1000. This can be over-ridden per stage."
  default     = "1000"
}

variable "opt_throttling_burst_limit" {
  description = "Set this to a number to specify the maximum burst requests to allow. Default is 250. This can be over-ridden per stage."
  default     = "250"
}

variable "domain_name" {
  description = "Set this attribute to the name of domain to map the API deployment to. The domain must already be available as a Route53 hosted zone, and an AWS Certficate **in `us-east-1` region** must be available for the domain name. Default is no domain."
  default     = ""
}

variable "domain_prefix" {
  description = "Set this to specify a prefix to the domain name set via `domain_name`. Do not include a terminating period (.) as module will append that if this property is set. And if set we will look for a wildcard certificate **in `us-east-1` region**. Default is no prefix."
  default     = ""
}

variable "stage_vars" {
  description = "Optional map of stage variables available to all stages in this deployment."
  type        = map(string)
  default     = {}
}

