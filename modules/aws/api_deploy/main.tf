data "aws_region" "current" {
}

locals {
  stage_list = keys(var.stages)

  first_stage = local.stage_list[0]

  add_stages = slice(local.stage_list, 1, length(local.stage_list))

  # Take the defaults from the global opts to the module.
  default_stage_options = {
    opt_cwmetrics              = var.opt_cwmetrics
    opt_cwlogging              = var.opt_cwlogging
    opt_throttling_rate_limit  = var.opt_throttling_rate_limit
    opt_throttling_burst_limit = var.opt_throttling_burst_limit
  }
}

#
# This is out here because of dependencies without which TF tries
# to make this prematurely
resource "aws_api_gateway_deployment" "deploy" {
  rest_api_id = var.api_id
  stage_name = lookup(
    var.stages[local.first_stage],
    "stage_name",
    local.first_stage,
  )
  variables = merge(
    var.stage_vars,
    {
      "STAGE_NAME" = lookup(
        var.stages[local.first_stage],
        "stage_name",
        local.first_stage,
      )
    },
  )
  stage_description = "${var.deploy_prefix} deployment stage ${lookup(
    var.stages[local.first_stage],
    "stage_name",
    local.first_stage,
  )}"
  description = join(", ", var.method_types)
  lifecycle {
    create_before_destroy = "true"
  }
}

#
# Enable logging and metrics for all methods in this deployment/stage
#
resource "aws_api_gateway_method_settings" "deploy" {
  rest_api_id = aws_api_gateway_deployment.deploy.rest_api_id
  stage_name  = aws_api_gateway_deployment.deploy.stage_name
  method_path = "*/*"

  settings {
    metrics_enabled = lookup(
      var.stages[local.first_stage],
      "opt_cwmetrics",
      local.default_stage_options["opt_cwmetrics"],
    ) == "yes"
    logging_level = lookup(
      var.stages[local.first_stage],
      "opt_cwlogging",
      local.default_stage_options["opt_cwlogging"],
    ) == "yes" ? "INFO" : "OFF"
    throttling_rate_limit = lookup(
      var.stages[local.first_stage],
      "opt_throttling_rate_limit",
      local.default_stage_options["opt_throttling_rate_limit"],
    )
    throttling_burst_limit = lookup(
      var.stages[local.first_stage],
      "opt_throttling_burst_limit",
      local.default_stage_options["opt_throttling_burst_limit"],
    )
  }
}

locals {
  // if specified, use the domain name prefix as a sub-domain.
  deploy_fqdn = var.domain_prefix == "" ? var.domain_name : "${var.domain_prefix}.${var.domain_name}"
  cert_domain = var.domain_prefix == "" ? var.domain_name : "*.${var.domain_name}"
}

// We need to make sure we have this cert in `us-east-1` for use with CloudFront.
provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

data "aws_acm_certificate" "deploy" {
  provider = aws.us-east-1
  count    = var.domain_name == "" ? 0 : 1
  domain   = local.cert_domain
  statuses = ["ISSUED"]
}

resource "aws_api_gateway_domain_name" "deploy" {
  count           = var.domain_name == "" ? 0 : 1
  domain_name     = local.deploy_fqdn
  certificate_arn = data.aws_acm_certificate.deploy[count.index].arn
}

#
# If domain name specified, then map it to the deployment stage
#
data "aws_route53_zone" "deploy" {
  count = var.domain_name == "" ? 0 : 1
  name  = var.domain_name
}

resource "aws_route53_record" "deploy" {
  count   = var.domain_name == "" ? 0 : 1
  zone_id = data.aws_route53_zone.deploy[count.index].id
  name    = aws_api_gateway_domain_name.deploy[count.index].domain_name
  type    = "A"

  alias {
    name                   = aws_api_gateway_domain_name.deploy[count.index].cloudfront_domain_name
    zone_id                = aws_api_gateway_domain_name.deploy[count.index].cloudfront_zone_id
    evaluate_target_health = false
  }
}

resource "aws_api_gateway_base_path_mapping" "deploy" {
  count       = var.domain_name == "" ? 0 : 1
  api_id      = var.api_id
  stage_name  = aws_api_gateway_deployment.deploy.stage_name
  base_path   = aws_api_gateway_deployment.deploy.stage_name
  domain_name = aws_route53_record.deploy[count.index].fqdn
}

# 
# Additional stages, if specified.
#
resource "aws_api_gateway_stage" "deploy_add_stages" {
  count       = length(local.add_stages)
  rest_api_id = aws_api_gateway_deployment.deploy.rest_api_id
  stage_name = lookup(
    var.stages[local.add_stages[count.index]],
    "stage_name",
    local.add_stages[count.index],
  )

  # stage_name    = "${local.add_stages[count.index]}"
  deployment_id = aws_api_gateway_deployment.deploy.id
  variables = merge(
    var.stage_vars,
    {
      "STAGE_NAME" = lookup(
        var.stages[local.add_stages[count.index]],
        "stage_name",
        local.add_stages[count.index],
      )
    },
  )
  description = "${var.deploy_prefix} deployment stage ${lookup(
    var.stages[local.add_stages[count.index]],
    "stage_name",
    local.add_stages[count.index],
  )}"
}

# And the settings for the additional stages if specified
#
resource "aws_api_gateway_method_settings" "deploy_add_stages" {
  count       = length(local.add_stages)
  rest_api_id = aws_api_gateway_deployment.deploy.rest_api_id
  stage_name  = aws_api_gateway_stage.deploy_add_stages[count.index].stage_name
  method_path = "*/*"

  settings {
    metrics_enabled = lookup(
      var.stages[local.add_stages[count.index]],
      "opt_cwmetrics",
      local.default_stage_options["opt_cwmetrics"],
    ) == "yes"
    logging_level = lookup(
      var.stages[local.add_stages[count.index]],
      "opt_cwlogging",
      local.default_stage_options["opt_cwlogging"],
    ) == "yes" ? "INFO" : "OFF"
    throttling_rate_limit = lookup(
      var.stages[local.add_stages[count.index]],
      "opt_throttling_rate_limit",
      local.default_stage_options["opt_throttling_rate_limit"],
    )
    throttling_burst_limit = lookup(
      var.stages[local.add_stages[count.index]],
      "opt_throttling_burst_limit",
      local.default_stage_options["opt_throttling_burst_limit"],
    )
  }
}

# And the domain name mappings for the additional stages, if specified
resource "aws_api_gateway_base_path_mapping" "deploy_add_stages" {
  count       = var.domain_name == "" ? 0 : length(local.add_stages)
  api_id      = var.api_id
  stage_name  = aws_api_gateway_stage.deploy_add_stages[count.index].stage_name
  base_path   = aws_api_gateway_stage.deploy_add_stages[count.index].stage_name
  domain_name = aws_route53_record.deploy[0].fqdn
}

