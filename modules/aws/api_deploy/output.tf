locals {
  # If domain name is specified, use the FQDN that was generated via the use of domain name (and optional prefix)
  # Else construct the host name using the API ID and current region.
  api_host = var.domain_name == "" ? "${var.api_id}.execute-api.${data.aws_region.current.name}.amazonaws.com" : local.deploy_fqdn

  api_deploy_url = var.domain_name == "" ? aws_api_gateway_deployment.deploy.invoke_url : "https://${local.deploy_fqdn}/${aws_api_gateway_deployment.deploy.stage_name}"
}

output "url" {
  description = "The API URL, based on the custom domain name if specified. *Deprecated*"
  value       = local.api_deploy_url
}

output "api_urls" {
  description = "The list of invocation URLs for for all the stages, including optional additional ones, using domain name if specified."

  value = concat(
    [local.api_deploy_url],
    formatlist(
      "https://${local.api_host}/%v",
      aws_api_gateway_stage.deploy_add_stages.*.stage_name,
    ),
  )
}

output "raw_urls" {
  description = "The list of naked invocation URLs for for all the stages, including optional additional ones."

  value = concat(
    [aws_api_gateway_deployment.deploy.invoke_url],
    aws_api_gateway_stage.deploy_add_stages.*.invoke_url,
  )
}

output "description" {
  description = "A description of the linked API methods associated with this deployment."
  value       = aws_api_gateway_deployment.deploy.description
}

