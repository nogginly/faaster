locals {
  //
  // the runtime map lets us map the language names to
  // supported Lambda runtimes
  runtime_map = {
    node   = "nodejs10.x"
    node10 = "nodejs10.x"
    node8  = "nodejs8.10"
    go     = "go1.x"
    go1    = "go1.x"
  }

  //
  // the base map lets us put the actual functions in the same language-specific
  // folder regardless of version suffix
  lang_base_map = {
    node   = "node"
    node10 = "node"
    node8  = "node"
    go     = "go"
    go1    = "go"
  }

  //
  // the lang name map lets us map the runtime backs to the proper
  // lang name
  lang_name_map = {
    "nodejs10.x" = "node10"
    "nodejs8.10" = "node8"
    "go1.x"      = "go1"
  }

  fn_lang_base = local.lang_base_map[var.fn_lang]
  fn_runtime   = local.runtime_map[var.fn_lang]

  fn_base_path = "${var.fn_class}${var.fn_class == "" ? "" : "/"}${local.fn_lang_base}${local.fn_lang_base == "go" ? "/src" : ""}"

  // Convert class to a safe name for use as part of the name of the function ZIP file by
  //    - replacing all '/' with '_'
  //    - appending '_' if class is not empty
  //
  class_as_name = "${replace(var.fn_class, "/", "_")}${var.fn_class == "" ? "" : "_"}"

  // Determine the name of the Lambda source folder, taking into account override via `fn_source` property.
  source_name = var.fn_source == "" ? var.fn_name : var.fn_source

  //
  // Regardless of source name, always use `fn_name` for the ZIP file per Lambda so we can keep them separate
  // Also include the reverse-mapped lang name based on runtime in the ZIP file name.
  zip_name = "${local.lang_name_map[local.fn_runtime]}_${var.fn_name}"

  default_env_vars = {
    DEPLOY_PREFIX = var.deploy_prefix
  }
}

data "archive_file" "lambda" {
  type        = "zip"
  source_dir  = "./functions/${local.fn_base_path}/${local.source_name}"
  output_path = "./functions/fun_${local.class_as_name}${local.zip_name}.zip" # Make ZIP files using `fn_name`.
}

resource "aws_lambda_function" "lambda" {
  function_name    = "${var.deploy_prefix}-${var.fn_name}-lambda"
  handler          = local.fn_lang_base == "node" ? "index.handler" : "handler"
  runtime          = local.fn_runtime
  memory_size      = var.fn_mem
  filename         = data.archive_file.lambda.output_path
  source_code_hash = data.archive_file.lambda.output_base64sha256
  role             = var.lambda_role
  timeout          = var.fn_timeout
  kms_key_arn      = var.key_arn

  environment {
    variables = merge(var.env_vars, local.default_env_vars)
  }
}

