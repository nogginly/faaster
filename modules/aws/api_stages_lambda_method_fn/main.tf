data "aws_region" "current" {
}

data "aws_caller_identity" "current" {
}

locals {
  //
  // the runtime map lets us map the language names to
  // supported Lambda runtimes
  runtime_map = {
    node   = "nodejs10.x"
    node10 = "nodejs10.x"
    node8  = "nodejs8.10"
    go     = "go1.x"
    go1    = "go1.x"
  }

  //
  // the base map lets us put the actual functions in the same language-specific
  // folder regardless of version suffix
  lang_base_map = {
    node   = "node"
    node10 = "node"
    node8  = "node"
    go     = "go"
    go1    = "go"
  }

  //
  // the lang name map lets us map the runtime backs to the proper
  // lang name
  lang_name_map = {
    "nodejs10.x" = "node10"
    "nodejs8.10" = "node8"
    "go1.x"      = "go1"
  }

  default_env_vars = {
    DEPLOY_PREFIX = var.deploy_prefix
  }

  stage_ids = keys(var.fn_stages)
}

#
# Per stage in the list of stage_ids
# Each Lambda function has a naming convention like so:
#
#   "fun_" + STAGE + "_" + NAME
#
# This allows us to define the API method so that it uses the 
# stage variable "STAGE_NAME" when defining the Lambda reference
#
# We also supply an env var to each Lambda that identifies the
# stage it's intended for, called
#
#   DEPLOY_STAGE
#
#
data "archive_file" "lambda" {
  count = length(local.stage_ids)
  type  = "zip"

  # Consider per-stage optional config for these Lambda properties before falling back to global config and their defaults
  source_dir  = "./functions/${lookup(var.fn_stages[local.stage_ids[count.index]], "stage_name", local.stage_ids[count.index])}/${lookup(local.lang_base_map, lookup(var.fn_stages[local.stage_ids[count.index]], "fn_lang", var.fn_lang))}${lookup(local.lang_base_map, lookup(var.fn_stages[local.stage_ids[count.index]], "fn_lang", var.fn_lang)) == "go" ? "/src" : ""}/${var.fn_name_prefix}"
  output_path = "./functions/fun_${lookup(var.fn_stages[local.stage_ids[count.index]], "stage_name", local.stage_ids[count.index])}_${lookup(local.lang_name_map, lookup(local.runtime_map, lookup(var.fn_stages[local.stage_ids[count.index]], "fn_lang", var.fn_lang)))}_${var.fn_name_prefix}.zip"
}

resource "aws_lambda_function" "lambda" {
  count            = length(local.stage_ids)
  function_name    = "${var.deploy_prefix}-${lookup(var.fn_stages[local.stage_ids[count.index]], "stage_name", local.stage_ids[count.index])}-${var.fn_name_prefix}"
  source_code_hash = data.archive_file.lambda[count.index].output_base64sha256
  role             = var.lambda_role
  kms_key_arn      = var.key_arn
  filename         = data.archive_file.lambda[count.index].output_path

  # Consider per-stage optional config for these Lambda properties before falling back to global config and their defaults
  handler     = lookup(local.lang_base_map, lookup(var.fn_stages[local.stage_ids[count.index]], "fn_lang", var.fn_lang)) == "node" ? "index.handler" : "handler"
  runtime     = lookup(local.runtime_map, lookup(var.fn_stages[local.stage_ids[count.index]], "fn_lang", var.fn_lang))
  memory_size = lookup(var.fn_stages[local.stage_ids[count.index]], "fn_mem", var.fn_mem)
  timeout     = lookup(var.fn_stages[local.stage_ids[count.index]], "fn_timeout", var.fn_timeout)

  environment {
    variables = merge(
      var.env_vars,
      local.default_env_vars,
      {
        "DEPLOY_STAGE" = lookup(
          var.fn_stages[local.stage_ids[count.index]],
          "stage_name",
          local.stage_ids[count.index],
        )
      },
    )
  }
}

resource "aws_lambda_permission" "allow_api_gateway" {
  count         = length(local.stage_ids)
  function_name = aws_lambda_function.lambda[count.index].function_name
  statement_id  = "AllowExecutionFromApiGateway"
  action        = "lambda:InvokeFunction"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${var.api_id}/*/${aws_api_gateway_method.api_method.http_method}${var.resource_path}"
}

#
# Define the API method and integration once using stage variable STAGE_NAME to refer to the
# Lambda function, which API gateway will substitute on invocation via the
# stage-specific endpoint
#
resource "aws_api_gateway_method" "api_method" {
  rest_api_id   = var.api_id
  resource_id   = var.resource_id
  http_method   = var.http_method
  authorization = var.auth_id == "" ? "NONE" : "CUSTOM"
  authorizer_id = var.auth_id
}

locals {
  fn_staged_var_arn = "arn:aws:lambda:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:function:${var.deploy_prefix}-$${stageVariables.STAGE_NAME}-${var.fn_name_prefix}"
}

resource "aws_api_gateway_integration" "api_method_integration" {
  rest_api_id             = var.api_id
  resource_id             = var.resource_id
  http_method             = aws_api_gateway_method.api_method.http_method
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/${local.fn_staged_var_arn}/invocations"
  integration_http_method = "POST"
}

