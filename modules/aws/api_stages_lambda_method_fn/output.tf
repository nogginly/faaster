output "type" {
  value = "${aws_api_gateway_integration.api_method_integration.http_method}${var.resource_path}"
}

output "function_versions" {
  description = "The list of per-stage Lambda function versions"
  value       = aws_lambda_function.lambda.*.version
}

output "function_names" {
  description = "The list of per-stage Lambda function names"
  value       = aws_lambda_function.lambda.*.function_name
}

output "function_arns" {
  description = "The list of per-stage Lambda function ARN's"
  value       = aws_lambda_function.lambda.*.arn
}

