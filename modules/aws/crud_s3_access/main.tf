// Policies

// DELETE
data "aws_iam_policy_document" "s3_delete_access" {
  statement {
    effect = "Allow"

    actions = [
      "s3:DeleteObject",
    ]

    resources = [
      "${var.bucket_arn}/${var.bucket_prefix}*",
    ]
  }
}

resource "aws_iam_policy" "s3_delete_access" {
  count  = var.delete_allow_count > 0 ? 1 : 0
  name   = "DeleteIn-${var.bucket_name}"
  policy = data.aws_iam_policy_document.s3_delete_access.json
}

// WRITE
data "aws_iam_policy_document" "s3_write_access" {
  statement {
    effect = "Allow"

    actions = [
      "s3:PutObject",
      "s3:PutObjectAcl",
    ]

    resources = [
      "${var.bucket_arn}/${var.bucket_prefix}*",
    ]
  }
}

resource "aws_iam_policy" "s3_write_access" {
  count  = var.write_allow_count > 0 ? 1 : 0
  name   = "WriteTo-${var.bucket_name}"
  policy = data.aws_iam_policy_document.s3_write_access.json
}

// READ/QUERY
data "aws_iam_policy_document" "s3_read_access" {
  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:GetObjectAcl",
    ]

    resources = [
      "${var.bucket_arn}/${var.bucket_prefix}*",
    ]
  }
}

resource "aws_iam_policy" "s3_read_access" {
  count  = var.read_allow_count > 0 ? 1 : 0
  name   = "ReadFrom-${var.bucket_name}"
  policy = data.aws_iam_policy_document.s3_read_access.json
}

// -------- Attach policy to Lambda methods by iterating through the lists and creating
//          attachment resources. 

resource "aws_iam_role_policy_attachment" "s3_read_access" {
  count      = var.read_allow_count
  role       = element(var.read_allow_roles, count.index)
  policy_arn = aws_iam_policy.s3_read_access[0].arn
}

resource "aws_iam_role_policy_attachment" "s3_write_access" {
  count      = var.write_allow_count
  role       = element(var.write_allow_roles, count.index)
  policy_arn = aws_iam_policy.s3_write_access[0].arn
}

resource "aws_iam_role_policy_attachment" "s3_delete_access" {
  count      = var.delete_allow_count
  role       = element(var.delete_allow_roles, count.index)
  policy_arn = aws_iam_policy.s3_delete_access[0].arn
}

