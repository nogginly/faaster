module "lambda_role" {
  source        = "../api_lambda_role"
  name          = "${var.fn_name_prefix}-STAGES"
  opt_cwlogging = var.opt_cwlogging
  deploy_prefix = var.deploy_prefix
}

module "lambda_method" {
  source         = "../api_stages_lambda_method_fn"
  api_id         = var.api_id
  resource_id    = var.resource_id
  resource_path  = var.resource_path
  deploy_prefix  = var.deploy_prefix
  fn_stages      = var.fn_stages
  fn_name_prefix = var.fn_name_prefix
  fn_lang        = var.fn_lang
  fn_timeout     = var.fn_timeout
  fn_mem         = var.fn_mem
  http_method    = var.http_method
  lambda_role    = module.lambda_role.role_arn # role per method
  auth_id        = var.auth_id
  key_arn        = var.key_arn
  env_vars       = var.env_vars
}

