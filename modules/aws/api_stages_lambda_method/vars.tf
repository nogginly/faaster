variable "api_id" {
  description = "The ID of the API as part of which this method is deployed."
}

variable "resource_id" {
  description = "The ID of the API resource to which this method is associated."
}

variable "resource_path" {
  description = "The full path of the API resource."
}

variable "deploy_prefix" {
  description = "A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only."
}

variable "fn_stages" {
  type = map(string)

  description = <<DOC
    Specify names of all the deployment stages for which to define Lambda functions as part of this multi-stage
    API method, where each key is a stage name and value is a map of optional config for the per-stage Lambda overriding
    any global config. 
    
    The stages are processed after sorting the keys in lexical order. You can use quoted numbers to preserve order and 
    use `stage_name` option to specify the stage name.

    Supported optional config properties are: `stage_name`, `fn_mem`, `fm_timeout`, `fn_lang`.

    E.g. 
    ```
    fn_stages = {
      "0" = {
        stage_name = "api"
        fn_mem = 128
      }
      "dev" {
        fn_lang = "go"
      }
    }
    
DOC

}

variable "fn_name_prefix" {
  description = "The name prefix of the Lambda functions, which will be combined with the stage name as suffix to define a Lambda function per stage in the list of stage names."
}

variable "fn_lang" {
  description = "The language for the function, which can be one of the following: \"node\", \"node10\", \"node8\", \"go\", \"go1\". Unversioned language names map to highest version."
  default     = "node"
}

variable "fn_timeout" {
  description = "The maximum time in seconds the function can run for. Default is 3s."
  default     = 3
}

variable "fn_mem" {
  description = "The amount of memory in MB for the Lambda function, in 64MB increments starting at 128MB. Defaults to 128."
  default     = 128
}

variable "http_method" {
  description = "The API method's HTTP method type. E.g. GET, POST etc."
}

variable "auth_id" {
  description = "The custom authorizer ID; accessible from the `api_lambda_auth` module outputs. If not specified, no authorization is used."
  default     = ""
}

variable "key_arn" {
  description = "The KMS key ARN for Lambda to use instead of the default service key provided by AWS."
  default     = ""
}

variable "env_vars" {
  description = "Optional map of environment variables to pass through to the Lambda function."
  type        = map(string)
  default     = {}
}

variable "opt_cwlogging" {
  description = "Set this attribute to 'no' to exclude Cloud Watch logging access for Lambda in the role. Enabled by default."
  default     = "yes"
}

