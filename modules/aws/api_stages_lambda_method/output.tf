output "type" {
  value = module.lambda_method.type
}

output "function_versions" {
  description = "The list of per-stage Lambda function versions"
  value       = module.lambda_method.function_versions
}

output "function_names" {
  description = "The list of per-stage Lambda function names"
  value       = module.lambda_method.function_names
}

output "function_arns" {
  description = "The list of per-stage Lambda function ARN's"
  value       = module.lambda_method.function_arns
}

output "role_id" {
  value = module.lambda_role.role_id
}

output "role_arn" {
  value = module.lambda_role.role_arn
}

