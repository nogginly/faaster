#
# This sets up an API in API Gateway and publishes endpoints (stages) for
# - production called "api" 
#

resource "aws_api_gateway_rest_api" "api" {
  name               = "${var.deploy_prefix}-${var.api_name}"
  description        = var.api_desc
  binary_media_types = var.binary_media_types
}

