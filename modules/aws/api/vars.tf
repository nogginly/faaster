variable "api_name" {
  description = "The name of the API under which the function is deployed."
}

variable "api_desc" {
  description = "The description of the API under which the function is deployed."
}

variable "binary_media_types" {
  description = "A list of binary content types that the API will return if you want to limit it in some way."
  type        = list(string)
  default     = ["*/*"]
}

variable "deploy_prefix" {
  description = "A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only."
}

