output "type" {
  value = "${aws_api_gateway_authorizer.authorizer.type}/auth"
}

output "auth_id" {
  value = aws_api_gateway_authorizer.authorizer.id
}

output "version" {
  value = module.lambda_fn.version
}

