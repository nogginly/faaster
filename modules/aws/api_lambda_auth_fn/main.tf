data "aws_region" "current" {
}

module "lambda_fn" {
  source        = "../lambda_fn"
  fn_name       = var.fn_name
  fn_source     = var.fn_source
  fn_class      = var.fn_class
  fn_lang       = var.fn_lang
  fn_mem        = var.fn_mem
  fn_timeout    = var.fn_timeout
  deploy_prefix = var.deploy_prefix
  key_arn       = var.key_arn
  env_vars      = var.env_vars
  lambda_role   = var.lambda_role
}

data "aws_caller_identity" "current" {
}

resource "aws_api_gateway_authorizer" "authorizer" {
  name                             = "${var.deploy_prefix}-${var.fn_name}"
  rest_api_id                      = var.api_id
  authorizer_uri                   = module.lambda_fn.invoke_arn
  authorizer_credentials           = aws_iam_role.auth_invocation_role.arn
  type                             = "TOKEN"
  authorizer_result_ttl_in_seconds = var.auth_ttl
}

resource "aws_iam_role" "auth_invocation_role" {
  name = "${var.deploy_prefix}-apig-auth-invoke-role"
  path = "/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_role_policy" "auth_invocation_policy" {
  name = "${var.deploy_prefix}-apig-auth-invoke-policy"
  role = aws_iam_role.auth_invocation_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "lambda:InvokeFunction",
      "Effect": "Allow",
      "Resource": "${module.lambda_fn.arn}"
    }
  ]
}
EOF

}

