module "lambda_role" {
  source        = "../api_lambda_role"
  name          = var.fn_name
  opt_cwlogging = var.opt_cwlogging
  deploy_prefix = var.deploy_prefix
}

module "lambda_auth" {
  source        = "../api_lambda_auth_fn"
  fn_class      = var.fn_class
  fn_name       = var.fn_name
  fn_source     = var.fn_source
  fn_timeout    = var.fn_timeout
  fn_lang       = var.fn_lang
  fn_mem        = var.fn_mem
  api_id        = var.api_id
  deploy_prefix = var.deploy_prefix
  lambda_role   = module.lambda_role.role_arn
  auth_ttl      = var.auth_ttl
  key_arn       = var.key_arn
  env_vars      = var.env_vars
}

