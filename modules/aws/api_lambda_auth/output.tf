output "type" {
  value = module.lambda_auth.type
}

output "role_id" {
  value = module.lambda_role.role_id
}

output "role_arn" {
  value = module.lambda_role.role_arn
}

output "auth_id" {
  value = module.lambda_auth.auth_id
}

output "version" {
  value = module.lambda_auth.version
}

