data "aws_region" "current" {
}

module "lambda_fn" {
  source        = "../lambda_fn"
  fn_name       = var.fn_name
  fn_source     = var.fn_source
  fn_class      = var.fn_class
  fn_lang       = var.fn_lang
  fn_mem        = var.fn_mem
  fn_timeout    = var.fn_timeout
  deploy_prefix = var.deploy_prefix
  key_arn       = var.key_arn
  env_vars      = var.env_vars
  lambda_role   = var.lambda_role
}

data "aws_caller_identity" "current" {
}

resource "aws_api_gateway_method" "api_method" {
  rest_api_id   = var.api_id
  resource_id   = var.resource_id
  http_method   = var.http_method
  authorization = var.auth_id == "" ? "NONE" : "CUSTOM"
  authorizer_id = var.auth_id
}

resource "aws_api_gateway_integration" "api_method_integration" {
  rest_api_id             = var.api_id
  resource_id             = var.resource_id
  http_method             = aws_api_gateway_method.api_method.http_method
  type                    = "AWS_PROXY"
  uri                     = module.lambda_fn.invoke_arn
  integration_http_method = "POST"
}

resource "aws_lambda_permission" "allow_api_gateway" {
  function_name = module.lambda_fn.arn
  statement_id  = "AllowExecutionFromApiGateway"
  action        = "lambda:InvokeFunction"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${var.api_id}/*/${aws_api_gateway_method.api_method.http_method}${var.resource_path}"
}

