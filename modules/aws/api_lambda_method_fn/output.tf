output "type" {
  value = "${aws_api_gateway_integration.api_method_integration.http_method}${var.resource_path}"
}

output "version" {
  value = module.lambda_fn.version
}

