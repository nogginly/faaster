variable "path_name" {
  description = "The name of the API path part"
}

variable "api_id" {
  description = "The ID of the API Gateway REST API under which this path is deployed."
}

variable "parent_id" {
  description = "The ID of the resource that will be the parent path under which this path is deployed. Can be the `root_id` of an API or the `id` of another resource."
}

