variable "elasticsearch_name" {
  description = "The name of the ElasticSearch domain which is used within the policy names."
}

variable "elasticsearch_arn" {
  description = "The ARN of the ElasticSearch domain associated with these access policies and attachment."
}

variable "elasticsearch_index" {
  default     = ""
  description = "The index within the ElasticSearch domain to which to limit this access policy. Default to any index."
}

variable "read_allow_count" {
  description = "The no. of Lambda role IDs to attach to the read access policy. Use 0 if none."
}

variable "write_allow_count" {
  description = "The no. of Lambda role IDs to attach to the write access policy. Use 0 if none."
}

variable "delete_allow_count" {
  description = "The no. of Lambda role IDs to attach to the delete access policy. Use 0 if none."
}

variable "read_allow_roles" {
  type        = list(string)
  description = "The Lambda role IDs to attach to the read access policy. Use an empty list if none."
}

variable "write_allow_roles" {
  type        = list(string)
  description = "The Lambda role IDs to attach to the write access policy. Use an empty list if none."
}

variable "delete_allow_roles" {
  type        = list(string)
  description = "The Lambda role IDs to attach to the delete access policy. Use an empty list if none."
}

