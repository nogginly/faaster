output "type" {
  value = module.lambda_method.type
}

output "role_id" {
  value = module.lambda_role.role_id
}

output "role_arn" {
  value = module.lambda_role.role_arn
}

output "version" {
  value = module.lambda_method.version
}

