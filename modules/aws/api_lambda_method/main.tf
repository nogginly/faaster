module "lambda_role" {
  source        = "../api_lambda_role"
  name          = var.fn_name
  opt_cwlogging = var.opt_cwlogging
  deploy_prefix = var.deploy_prefix
}

module "lambda_method" {
  source        = "../api_lambda_method_fn"
  fn_name       = var.fn_name
  fn_class      = var.fn_class
  fn_source     = var.fn_source
  fn_timeout    = var.fn_timeout
  fn_lang       = var.fn_lang
  fn_mem        = var.fn_mem
  http_method   = var.http_method
  api_id        = var.api_id
  resource_id   = var.resource_id
  resource_path = var.resource_path
  deploy_prefix = var.deploy_prefix
  lambda_role   = module.lambda_role.role_arn
  auth_id       = var.auth_id
  key_arn       = var.key_arn
  env_vars      = var.env_vars
}

