variable "api_id" {
  description = "The ID of the API as part of which this method is deployed."
}

variable "resource_id" {
  description = "The ID of the API resource to which this method is associated."
}

variable "resource_path" {
  description = "The full path of the API resource."
}

variable "deploy_prefix" {
  description = "A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only."
}

variable "fn_class" {
  description = "This optional class name is embedded in the path to find the function code as follows: `functions/<class>/<fn_lang>/...`"
  default     = ""
}

variable "fn_name" {
  description = "The name of the Lambda function, which is also the name of the folder with the Lambda handler. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder."
}

variable "fn_source" {
  description = "Optional. Use this to specify the name of the folder with the Lambda handler if it's different from the name of the Lambda function. Depending on the language, this module looks for the named folder in `functions/node` or `functions/go/src` folder."
  default     = ""
}

variable "fn_timeout" {
  description = "The maximum time in seconds the function can run for. Default is 3s."
  default     = 3
}

variable "fn_lang" {
  description = "The language for the function, which can be one of the following: \"node\", \"node10\", \"node8\", \"go\", \"go1\". Unversioned language names map to highest version."
  default     = "node"
}

variable "fn_mem" {
  description = "The amount of memory in MB for the Lambda function, in 64MB increments starting at 128MB. Defaults to 128."
  default     = 128
}

variable "http_method" {
  description = "The API method's HTTP method type. E.g. GET, POST etc."
}

variable "key_arn" {
  description = "The KMS key ARN for Lambda to use instead of the default service key provided by AWS."
  default     = ""
}

variable "opt_cwlogging" {
  description = "Set this attribute to 'yes' to include Cloud Watch logging access for Lambda in the role."
  default     = "no"
}

variable "auth_id" {
  description = "The custom authorizer ID; accessible from the `api_lambda_auth` module outputs. If not specified, no authorization is used."
  default     = ""
}

variable "env_vars" {
  description = "Optional map of environment variables to pass through to the Lambda function."
  type        = map(string)
  default     = {}
}

