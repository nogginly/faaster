data "aws_region" "current" {
}

data "aws_caller_identity" "current" {
}

// Define the cron schedule rule to generate events
resource "aws_cloudwatch_event_rule" "cron_rule" {
  name_prefix         = var.deploy_prefix
  schedule_expression = var.sched_expr
}

module "lambda_fn" {
  source        = "../lambda_fn"
  fn_name       = var.fn_name
  fn_source     = var.fn_source
  fn_class      = var.fn_class
  fn_lang       = var.fn_lang
  fn_mem        = var.fn_mem
  fn_timeout    = var.fn_timeout
  deploy_prefix = var.deploy_prefix
  key_arn       = var.key_arn
  env_vars      = var.env_vars
  lambda_role   = aws_iam_role.lambda_cron.arn
}

// Connect the event rule to the lambda function to execute/target
resource "aws_cloudwatch_event_target" "cron_target" {
  rule = aws_cloudwatch_event_rule.cron_rule.name
  arn  = module.lambda_fn.arn
}

resource "aws_iam_role" "lambda_cron" {
  name = "${var.deploy_prefix}-${var.fn_name}-iam-lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
            "lambda.amazonaws.com",
            "events.amazonaws.com" ]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_lambda_permission" "allow_events" {
  function_name = module.lambda_fn.arn
  statement_id  = "AllowExecutionFromEvents"
  action        = "lambda:InvokeFunction"
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.cron_rule.arn
}

data "aws_iam_policy_document" "logging" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = [
      "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
}

resource "aws_iam_role_policy" "cwlogs_access" {
  count  = var.opt_cwlogging == "yes" ? 1 : 0
  name   = "${var.deploy_prefix}-cwlogs-access-policy"
  role   = aws_iam_role.lambda_cron.id
  policy = data.aws_iam_policy_document.logging.json
}

