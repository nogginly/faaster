output "fn_version" {
  value = module.lambda_fn.version
}

output "role_id" {
  value = aws_iam_role.lambda_cron.id
}

output "role_arn" {
  value = aws_iam_role.lambda_cron.arn
}

