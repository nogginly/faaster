// Policies

// DELETE
data "aws_iam_policy_document" "db_delete_access" {
  statement {
    effect = "Allow"

    actions = [
      "dynamodb:DeleteItem",
    ]

    resources = [
      var.table_arn,
    ]
  }
}

resource "aws_iam_policy" "db_delete_access" {
  name   = "DeleteIn-${var.table_name}"
  policy = data.aws_iam_policy_document.db_delete_access.json
}

// WRITE 
data "aws_iam_policy_document" "db_write_access" {
  statement {
    effect = "Allow"

    actions = [
      "dynamodb:PutItem",
      "dynamodb:UpdateItem",
      "dynamodb:BatchWriteItem",
    ]

    resources = [
      var.table_arn,
    ]
  }
}

resource "aws_iam_policy" "db_write_access" {
  name   = "WriteTo-${var.table_name}"
  policy = data.aws_iam_policy_document.db_write_access.json
}

// READ/QUERY
data "aws_iam_policy_document" "db_read_access" {
  statement {
    effect = "Allow"

    actions = [
      "dynamodb:GetItem",
      "dynamodb:BatchGetItem",
      "dynamodb:ConditionCheckItem",
    ]

    resources = [
      var.table_arn,
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "dynamodb:Query",
      "dynamodb:Scan",
    ]

    resources = [
      "${var.table_arn}/*",
    ]
  }
}

resource "aws_iam_policy" "db_read_access" {
  name   = "ReadFrom-${var.table_name}"
  policy = data.aws_iam_policy_document.db_read_access.json
}

// -------- Attach policy to Lambda methods by iterating through the lists and creating
//          attachment resources. 

resource "aws_iam_role_policy_attachment" "db_read_access" {
  count      = var.read_allow_count
  role       = element(var.read_allow_roles, count.index)
  policy_arn = aws_iam_policy.db_read_access.arn
}

resource "aws_iam_role_policy_attachment" "db_write_access" {
  count      = var.write_allow_count
  role       = element(var.write_allow_roles, count.index)
  policy_arn = aws_iam_policy.db_write_access.arn
}

resource "aws_iam_role_policy_attachment" "db_delete_access" {
  count      = var.delete_allow_count
  role       = element(var.delete_allow_roles, count.index)
  policy_arn = aws_iam_policy.db_delete_access.arn
}

