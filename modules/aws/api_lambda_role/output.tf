output "role_id" {
  value = aws_iam_role.lambda_exec.id
}

output "role_arn" {
  value = aws_iam_role.lambda_exec.arn
}

