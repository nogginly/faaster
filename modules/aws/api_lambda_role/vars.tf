variable "name" {
  description = "Unique name for the role. When defining multiple lambda execution roles, make the names diatinctive to help with troubleshooting."
}

variable "deploy_prefix" {
  description = "A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only."
}

variable "opt_cwlogging" {
  description = "Set this attribute to 'yes' to include Cloud Watch logging access for Lambda in the role."
  default     = "no"
}

