data "aws_region" "current" {
}

data "aws_caller_identity" "current" {
}

resource "aws_iam_role" "lambda_exec" {
  name = "${var.deploy_prefix}-${var.name}-iam-lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
            "lambda.amazonaws.com",
            "apigateway.amazonaws.com" ]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

data "aws_iam_policy_document" "logging" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = [
      "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
}

resource "aws_iam_role_policy" "cwlogs_access" {
  count  = var.opt_cwlogging == "yes" ? 1 : 0
  name   = "${var.deploy_prefix}-cwlogs-access-policy"
  role   = aws_iam_role.lambda_exec.id
  policy = data.aws_iam_policy_document.logging.json
}

